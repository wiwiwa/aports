# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwrited
pkgver=5.26.0
pkgrel=0
pkgdesc="KDE daemon listening for wall and write messages"
arch="all !armhf" # qt5-qtdeclarative-dev  unavilable on armhf
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	knotifications-dev
	kpty-dev
	qt5-qtbase-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kwrited-$pkgver.tar.xz"
options="!check" # No tests available

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
93086730d311ab7f4b800a22eea09b6fbf7beff15506393d80b65e3219b972b153e9cd7350a96a59e7720f0e66d198434190e9af1bc34c0bf76aeefa81f50e09  kwrited-5.26.0.tar.xz
"
