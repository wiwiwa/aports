# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Leonardo Arena <rnalrd@alpinelinux.org>
pkgname=weechat
pkgver=3.7
pkgrel=0
pkgdesc="A fast, light, extensible ncurses-based chat client"
url="https://weechat.org"
arch="all"
options="!check" # test suite runs "sudo make install"
license="GPL-3.0-or-later"
depends_dev="asciidoctor
	cmake
	gettext-dev
	ncurses-dev
	gnutls-dev
	libgcrypt-dev
	curl-dev
	aspell-dev
	lua-dev
	perl-dev
	python3-dev
	ruby-dev
	zlib-dev
	zstd-dev
	"

makedepends="$depends_dev"
subpackages="$pkgname-dev
	$pkgname-doc
	$pkgname-lang
	$pkgname-spell:_plugin
	$pkgname-lua:_plugin
	$pkgname-perl:_plugin
	$pkgname-python:_plugin
	$pkgname-ruby:_plugin
	"

source="https://www.weechat.org/files/src/weechat-$pkgver.tar.gz"

# secfixes:
#   1.7.1-r0:
#     - CVE-2017-8073
#   1.9.1-r0:
#     - CVE-2017-14727
#   2.7.1-r0:
#     - CVE-2020-8955

build() {
	mkdir -p build
	cd build
	cmake .. \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DENABLE_MAN=ON \
		-DENABLE_TCL=OFF \
		-DENABLE_GUILE=OFF \
		-DENABLE_JAVASCRIPT=OFF \
		-DENABLE_PHP=OFF
	make
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir/" install
}

_plugin() {
	local _name=${subpkgname#*-}
	local _dir=usr/lib/weechat/plugins

	pkgdesc="WeeChat $_name plugin"
	depends="weechat"

	# as of 2.5 aspell has been renamed to spell on weechat upstream
	# since it now supports enchant as well, rename the subpackage and
	# replace the old one
	if [ "$_name" = spell ]; then
		replaces="$pkgname-aspell"
		provides="$pkgname-aspell=$pkgver-r$pkgrel"
	fi

	mkdir -p "$subpkgdir"/$_dir
	mv "$pkgdir"/$_dir/"$_name".so "$subpkgdir"/$_dir
}

check() {
	./tools/build-test.sh cmake
}

sha512sums="
d4e899deec68c9b0c894bd961fc17c6676918076df59d83341f02b6ad5a54a6b1e472122116cc3175a5a552eb09e9fbf624fecc81f3b41ac48abd6e2b92f38d9  weechat-3.7.tar.gz
"
