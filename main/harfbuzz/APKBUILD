# Contributor: Sören Tempel <soeren+alpinelinux@soeren-tempel.net>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=harfbuzz
pkgver=5.3.0
pkgrel=0
pkgdesc="Text shaping library"
url="https://freedesktop.org/wiki/Software/HarfBuzz"
arch="all"
license="MIT"
makedepends="
	cairo-dev
	freetype-dev
	glib-dev
	gobject-introspection-dev
	graphite2-dev
	gtk-doc
	icu-dev
	meson
	"
checkdepends="python3"
subpackages="$pkgname-static $pkgname-dev $pkgname-icu $pkgname-utils $pkgname-doc"
source="https://github.com/harfbuzz/harfbuzz/releases/download/$pkgver/harfbuzz-$pkgver.tar.xz"

# secfixes:
#   4.4.1-r0:
#     - CVE-2022-33068

case "$CARCH" in
ppc64le)
	# ERROR: hash for expected and actual does not match.
	options="$options !check"
	;;
esac

build() {
	abuild-meson \
		--default-library=both \
		-Dglib=enabled \
		-Dgobject=enabled \
		-Dgraphite=enabled \
		-Dicu=enabled \
		-Dfreetype=enabled \
		-Dcairo=enabled \
		-Ddocs=enabled \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

icu() {
	pkgdesc="Harfbuzz ICU support library"
	replaces="harfbuzz"

	amove usr/lib/lib*icu.so.*
}

utils() {
	pkgdesc="$pkgdesc (utilities)"

	amove usr/bin
}

sha512sums="
0eae94769ac0157bf1df9b001223b30d40104ac448b1062f34ac793edb27ed1e459ac99954498c4b3cfb65c8cda29f2214a300f11523826b378ab67deec1a5f3  harfbuzz-5.3.0.tar.xz
"
